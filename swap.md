La swap es un espacio en disco reservado exclusivamente para almacenar páginas de memoria de aplicaciones/servicios inactivos o en reposo. 
Más específicamente, es un método para hacer igualitaria la liberación de memoria (RAM) entre los diversos tipos de páginas de memoria (*file memory*, *anon memory*, *dirty page*, *clean page*, etc) cuando esta es reclamada por una aplicación activa. Más al respecto en este [post de 2018](https://chrisdown.name/2018/01/02/in-defence-of-swap.html).
También sirve para almacenar la imagen del sistema e *hibernar* (siempre que cuente con tamaño suficiente).
Además ayuda a evitar que el sistema se congele por saturación de la RAM.

Hay variantes para la creación de un espacio de swap:

* Un archivo 
* Una partición
* zswap

La lectura al disco es mucho más lenta que la lectura a la memoria.
Como alternativa al swap existe zram.
