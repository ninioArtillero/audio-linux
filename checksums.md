 La verificación de checksums permite comprobar la integridad de un archivo. Dos sistemas de encriptación populares son md5 y sha256. 

## Ejemplo: ISO de AV Linux

Al descargar AV Linux se encuentran los siguientes archivos:

* AV_Linux_MX_Edition-21.1_ahs_x64.iso
* AV_Linux_MX_Edition-21.1_ahs_x64.iso.md5
* AV_Linux_MX_Edition-21.1_ahs_x64.iso.sha256
* AV_Linux_MX_Edition-21.1_ahs_x64.iso.sig

Para cada checksum hay que comparar el resultado con el contenido de los archivos con terminación .sha256 y .md5, aunque se puede hacer de manera automática en el caso de que los archivos estén nombrados de manera apropiada:

```[bash]
$ md5sum --check AV_Linux_MX_Edition-21.1_ahs_x64.iso.md5

$ sha256sum --check AV_Linux_MX_Edition-21.1_ahs_x64.iso.sha256 
```

La firma con gpg ayuda a autenticar el autor del archivo imagen.
Lo primero es descargar la llave de cifrado del autor.
Para el caso de AV Linux utilizamos el siguiente comando:

`gpg --keyserver hkps://keyserver.ubuntu.com --recv-keys 0xDF1084F762F14A43`

Así tenemos la llave correspondiente guardada en el sistema, y podemos utilizar el archivo .sig (llamado apropiadamente para coincidir con el archivo a verificar) en el siguiente comando:

`gpg --verify AV_Linux_MX_Edition-21.1_ahs_x64.iso.sig`

Como siguiente argumento puede incluirse el nombre del archivo a verificar.

`gpg --verify AV_Linux_MX_Edition-21.1_ahs_x64.iso.sig AV_Linux_MX_Edition-21.1_ahs_x64.iso`
