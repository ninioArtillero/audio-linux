# Configuraciones para Audio

Listado de configuraciones de parámetros relacionados con el audio
y el procesamiento en tiempo real en algunas distribuciones.

2022 Agosto

# Kernel & Parameters

## Boot parameters / Comand line parameters

Parámetros de inicio del kernel, típicamente inicializados por grub y
definidos en /etc/default/grub como argumentos de GRUB_CMDLINE_LINUX_DEFAULT=""

### AV Linux:

audit=0 inter_pstate=disable hpet=disable quiet threadirqs splash

### Ubuntu Studio:
 
Kernel: 5.15.0-43-lowlatency x86_64 bits: 64 compiler: gcc v: 11.2.0
    parameters: BOOT_IMAGE=/boot/vmlinuz-5.15.0-43-lowlatency
        root=UUID=82e9a3c4-3d99-4faf-a3b7-458ae3c42479 ro threadirqs quiet splash


### Garuda

quiet splash rd.udev.log_priority=3 vt.global_cursor_default=0 systemd.unified_cgroup_hierarchy=1 resume=UUID=65b1555e-e8e7-41cb-84a2-63265cdbd7db loglevel=3 ibt=off # Modified by garuda-migrations: ibt=off sysrq_always_enabled=1"

## Swap

### AV Linux

swappiness=10
cache-pressure=100

ID-1: swap-1 type: partition size:1024 MiB priority: -2
dev: /dev/sda2

### Garuda

swappiness=133
cache-pressure=100

ID-1: swap-1 type: zram size: 8 GiB priority: 100
dev: /dev/zram0
ID-2: swap-2 type: partition size: 4 GiB priority: -2
dev: /dev/nvme0n1p2

# Filesystem

Los sistemas de archivos tambien tienen consecuencias en el performance del sistema.

## fstab

Aquí se definen los dispositivos a montar durante el inicio del sistema. Cada sistema de archivos cuenta con optiones de montaje para optimizar el rendimiento.

### AV Linux (Virtual Machine)

UUID= / ext4 noatime 1 1
UUID= swap swap defaults 0 0

### Garuda Linux

UUID= / btrfs subvol=/@,defaults,noatime,compress=zstd,discard=async,ssd 0 0
UUID= /home btrfs subvol=/@home,defaults,noatime,compress=zstd,discard=async,ssd 0 0
UUID= /root btrfs subvol=/@root,defaults,noatime,compress=zstd,discard=async,ssd 0 0
UUID= /srv btrfs subvol=/@srv,defaults,noatime,compress=zstd,discard=async,ssd 0 0
UUID= /var/cache btrfs subvol=/@cache,defaults,noatime,compress=zstd,discard=async,ssd 0 0
UUID= /var/log btrfs subvol=/@log,defaults,noatime,compress=zstd,discard=async,ssd 0 0
UUID= /var/tmp btrfs subvol=/@tmp,defaults,noatime,compress=zstd,discard=async,ssd 0 0

UUID= swap swap defaults,noatime 0 0
tmpfs /tmp tmpfs defaults,noatime,mode=1777 0 0

# Audio

## Garuda

Device-1: NVIDIA GP108 High Definition Audio vendor: ASUSTeK
driver: snd_hda_intel v: kernel bus-ID: 1-9:7 chip-ID: 0499:1509 pcie:
class-ID: ff00 gen: 3 speed: 8 GT/s lanes: 4 bus-ID: 07:00.1
chip-ID: 10de:0fb8 class-ID: 0403
Device-2: AMD Starship/Matisse HD Audio vendor: ASUSTeK
driver: snd_hda_intel v: kernel pcie: gen: 4 speed: 16 GT/s lanes: 16
bus-ID: 09:00.4 chip-ID: 1022:1487 class-ID: 0403
Device-3: Yamaha Steinberg UR22 type: USB driver: snd-usb-audio
Device-4: Logitech Webcam C270 type: USB driver: snd-usb-audio,uvcvideo
bus-ID: 3-2:3 chip-ID: 046d:0825 class-ID: 0102 serial: <filter>
Sound Server-1: ALSA v: k5.18.16-zen1-1-zen running: yes
Sound Server-2: PulseAudio v: 16.1 running: no
Sound Server-3: PipeWire v: 0.3.56 running: yes

## AV Linux (Virtual Machine)

Device-1: Intel 82801AA AC97 Audio vendor: Dell 
driver: snd_intel8x0 v: kernel bus-ID: 00:05.0 chip-ID: 8086:2415 class-ID: 0401 
Sound Server-1: ALSA v: k5.16.0-18.1-liquorix-amd64 running: yes 
Sound Server-2: JACK v: 1.9.21 running: no 
Sound Server-3: PulseAudio v: 14.2 running: yes 



