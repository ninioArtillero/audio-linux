Los parámetros del kernel permiten definir diversas opciones para el funcionamiento del mismo. [Aquí](https://www.kernel.org/doc/html/latest/admin-guide/kernel-parameters.html) hay una lista de los parámetros disponibles.

En las siguientes ubicaciones se pueden añadir archivos que definan o sobreescriban parámetros del kernel:

* `/etc/default/grub`
Aquí se definen los [parámetros del kernel cargados por Grub (bootloader)](https://wiki.archlinux.org/title/Kernel_parameters#GRUB)

* `/etc/sysctl.d/`
Los últimos archivos, en orden lexicográfico, sobrescriben a los anteriores en este directorio; por esto se suele iniciar el nombre con un número para ordenarlos. Por ejemplo, aquí se puede crear un archivo `99-swappiness.conf`  para modificar la [swappiness](https://wiki.archlinux.org/title/Swap#Swappiness)

* `/usr/lib/sysctl.d/`
Esta es otra ubicación en que se pueden modificar los parámetros del kernel.
Por ejemplo en Garuda Linux, la swappiness y el [sysrq](https://wiki.archlinux.org/title/Keyboard_shortcuts#Kernel_(SysRq)) se encuentran especificados en `99-sysctl-garuda.conf`

* `/etc/security/limits.d/`  
Aquí se establecen algunos permisos para grupos y usuarios. Se puede añadir un archivo `98-audio.conf` para otorgar privilegios de tiempo real al grupo "audio".


