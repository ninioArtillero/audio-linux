[Wireplumber](https://www.collabora.com/news-and-blog/blog/2020/05/07/wireplumber-the-pipewire-session-manager/) es un administrador de sesión para PipeWire. Su trabajo es establecer conexiones entre diversos nodos, para intercambiar flujos de audio y video.

Implementa la funcionalidad de Pulseaudio encargada de la selección automática de dispositivos. Funciona como un cliente de JACK, otorgando la flexibilidad de conexión para audio profesional.
