 Antes de instalar una distribución de Linux, es recomendable verificar checksums y signatures para asegurarse que la imagen del sistema no está corrupta.

Para copiar la ISO en una usb drive *booteable* es recomendado por muchos sitios utilizar [Balena Etcher](https://www.balena.io/etcher/). 
  * [ ] 
## Comando *dd*

Por otro lado es posible usar un comando nativo en Linux:

`dd if=/patch/to/file.iso of=/dev/sdX bs=4M status=progress conv=fsync`

Reemplazando X por la correspondiente ubicación del usb drive (checar con `lsblk`). 

CUIDADO: hay que estar seguro de la selección del disco a utilizar, pues este comando puede borrar completamente el sistema de archivos del sistema.

La sintaxis de este comando es "command option=value".
La opción "bs", asigna el block size de lectura y escritura en bytes (por defecto 512).
La opción "status=progress" hace que se muestre el progreso de la operación.
La opción "conv=fsync" evita que se indique que el proceso termino antes de que finalicen los procesos en segundo plano.
