 #  Lista de aplicaciones para Linux Desktop

Utilidades:

* Doom Emacs - editor de texto gráfico / entorno de desarrollo. Cuenta con un paquete de lenguaje para Tidal Cycles.
* Timeshift - imágenes del sistema (en caso de que este se rompa)
	* Snapper - Alternativa para imágenes del sistema
* Gparted - administrar discos y particiones

Para la línea de comandos:

* Neovim - editor de texto para terminal
* [rclone](https://rclone.org/) - sincronización de documentos en nube
	* Configurar [Google Drive](https://rclone.org/drive), otorgando acceso a la cuenta en este [link](https://console.cloud.google.com/), y [Mega]()

Web:
* qBittorrent - cliente torrent
* Brave - navegador enfocado a la privacidad y bloqueo de anuncios
* Thunderbird - cliente de email
* Element - cliente de matrix
* Zoom - videoconferencia

Multimedia

* mpv - reproductor
* Stremio - buscador de contenido vía stream

Investigación y oficina:

* Obsidian - toma de notas inteligentes para administrar Zettelkasten
* TexMaker - documentos LaTex
* Zotero - asistente de investigación
* Libre Office - suite de oficina (existe integración con Zotero)

Aplicaciones audio:

* Reaper - DAW
* Non-daw - aplicaciones para entorno de producción modular
* Agordejo - administrador de sesiones JACK
* ZynAddSubFX -  sintetizador aditivo y sustractivo
* Vital - sintetizador wavetable
* SuperCollider - programación de audio y composición algorítmica
* Pure Data - programación visual de audio y composición
* Sonic Pi - live coding con sintaxis tipo ruby
* Tidal Cycles - manipulación de patrones para live coding, escrito en Haskell

Aplicaciones visuales:

* GIMP - manipulación de imágenes
* xournalpp - firma de documentos y pizarrón
* Okular - lector de documentos

Performance tools:

* zram-generator - permite crear un disco virtual en la memoria para agilizar la swap
* ananicy-cpp - optimizador del CPU para una colección de aplicaciones de escritorio
