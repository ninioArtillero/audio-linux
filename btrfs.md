## Mantenimiento

Btrfs necesita mantenimiento, para reorganizar los bloques en que almacena los datos. Conforme pasa el tiempo, o se hacen movimientos grandes de archivos, es necesario realizar procesos como *balance, scrub, trim y defrag*.

Un balance completo en una SSD puede causar mucho desgaste en el disco, 
por lo que es recomendado utilizar filtros para trabajar sobre cierto porcentaje del mismo:

`bash -c "sudo btrfs balance start -musage=60 -dusage=60 / & sudo watch -t -n5 btrfs balance status / &&  fg"`

[*btrfsmaintenance*](https://github.com/kdave/btrfsmaintenance) es un conjunto de scripts, y unos *daemons* de *systemd*, para agendar automáticamente las actividades de mantemiento. Tambien @dalto ha desarrolla una GUI para manejar estas actividades y ver estadísticas, cuenta con integración para *snapshots* y también puede servir como *frontend* para *btrfsmaintenance*. La aplicación se llama [*btrfs-assistant*](https://gitlab.com/btrfs-assistant).

Algunas [recomendaciones](https://forum.garudalinux.org/t/troubleshooting-system-stutter-lags-freezes-and-hangs/18044/6s) de @tbg en el foro de Garuda Linux.
