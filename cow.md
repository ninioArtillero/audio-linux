## Copy on Write

Es una técnica para el manejo de recursos utilizada en la programación computacional para implementar eficientemente una operación de copia o duplicado en un recurso modificable ([Wiki](https://wiki.archlinux.org/title/Improving_performance)).

Si un recurso es duplicado, pero no modificado, no es necesario crear un nuevo recurso: el mismo puede ser compartido por original y copia. La modificaciones sí crean una copia; el nombre viene de que la operación es pospuesta hasta la primera escritura (*write*).

Uso principal: compartir memoria virtual para los procesos de un sistema operativo.
