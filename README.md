# Audio y Producción Musical en Linux

En este repositorio documentaré mi investigación en componentes, aplicaciones y esquemas relacionados con el audio y la producción musical en el escritorio Linux. 

Discutiré la configuración de un sistema linux para producción músical.
Esto implica optimizar el uso del *hardware* (prioridades del CPU) y *software* (servidor de audio) para este objetivo. 

La reproducción de audio en general es un proceso que no requiere mucha potencia del CPU. 
Sin embargo, tiene la necesidad de que su procesamiento no sea retardado por otros procesos (para no tener interrupciones en la reproducción o *xruns*). 
Esto se complica en entornos de producción en que se cargan muchas aplicaciones y plugins para ser ejecutados simultáneamente, 
sea para producción o interpretación en vivo la no interrupción del audio es fundamental para el flujo de trabajo.
Este problema no es necesariamente resuelto comprando mejor *hardware*. Desde hace años contamos con potencia suficiente en los procesadores para computadoras personales para llevar a cabo la tarea.
Aquí tomaré el enfoque de optimizar el sistema Linux.

Para abordar este problema es imperativo considerar el sistema como un todo, buscando mejorar del rendimiento global.
Unas referencias prácticas importantes sobre este tema son este artículo sobre el [performance](https://wiki.archlinux.org/title/Improving_performance) y este sobre el [audio profesional](https://wiki.archlinux.org/title/Professional_audio), ambos de la Arch Wiki.
Si bien contienen procedimientos específicos para Arch Linux, los mismos principios operan en otras distribuciones de Linux.

Utilizo la distribución [Garuda Linux](https://garudalinux.org/), por otorgar las ventajas de un sistema basado en Arch Linux, por el desarrollo de varias herramientas para el mantenimiento del mismo y la implementación de diversas tecnologías y configuraciones para optmizar el rendimiento del sistema.

## Hardware

1. CPU power profile
2. Privilegios de tiempo real
	1. IRQ 
3. Swappiness
	1. Swap

## Software

1. ALSA: software framework, part of Linux kernel.
2. Sound Server
	1. ALSA
	2. Jack
	3. Pulseaudio
	4. Pipewire
		1. Wireplumber
3. Filesystem:
    1. Journaling
        * ext4
        * xfs
   2. Copy-on-Write
        * zfs
        * btrfs
