Existen dos herramientas referenciadas en diversos sitios para evaluar las configuraciones para audio en tiempo real de un sistema linux.

En Arch Linux estos corresponden a los siguientes paquetes:

* `realtimeConfigQuickScan` - *realtimeconfigquickscan-git* en el AUR: Linux configuration checker for systems to be used for real-time audio.
* `realtime-suggestions` - *realtime-suggestions* en el AUR - Script suggesting improvements for realtime audio.
* `rtcqs` - *rtcqs* en el AUR - A Python utility to analyze your system and detect possible bottlenecks that could have a negative impact on the performance of your system when working with Linux audio.

Entre las configuraciones que revisan estas herramientas, las más notables son:

* Si perfil energético de los núcleos del procesador (_**CPU Governor**_) está configurado en *performance*. Manipulable con la herramienta CLI `cpupower`.
* Si el parámetro de **swappiness** (en las configuraciones de PAM limits) es $\leq 10$. Se puede verificar corriendo `cat /proc/sys/vm/swappiness`. Configurable creando un archivo de configuración en `/etc/sysctl.d/*.conf`.
	* En Garuda Linux está configuración se encuentra en `/usr/lib/sysctl.d/99-sysctl-garuda.conf`. Se puede sobreescribir con un archivo de configuración posterior, lexicográficamente, en la misma ubicación.
* Si existe un único __grupo de audio__ y el usuario está en el. En Arch Linux los privilegios de tiempo real están dados por el **grupo realtime**, creado y configurado por el paquete *realtime-privileges*.
	* Añadir archivo de configuración en `/etc/security/limits.d/*.conf` para tener acceso ilimitado a la memoria y alta prioridad.
	* En AV Linux, el archivo `/etc/security/limits.d/audio.conf` incluye los siguientes parámetros:
		* @audio - rtprio 95
		* @audio - memlock unlimited
		* opcional: @audio - nice -19
	* Con el grupo de realtime, se tienen las siguientes en `/etc/security/limits.d/99-realtime-privileges.conf`
		* @realtime - rtprio 98
		* @realtime - rtprio unlimited
		* @realtime - nice -11
* Si el kernel tiene _**Real-Time Preemption**_ (compilado con el realtime patch-set) o tiene activado en parámetro *threadirqs*.
	* En sistemas con *grub* este parámetro puede ser añadido en la la linea `GRUB_CMDLINE_LINUX_DEFAULT` de `/etc/default/grub` y ejecutando `grub-mkconfig`.

El script realtime-suggestions sugiere además instalar otras utilidades para el monitoreo y configuración de procesos input-ouput, scheduling y threading:

 * cyclictest - En *rt-tests* en Arch: A collection of latency testing tools for the linux(-rt) kernel.
 * iostat -En *sysstat* en Arch: A collection of performance monitoring.tools.
 * iotop - *iotop* en Arch: View I/O usage of processes.
 * rtirq - *rtirq* en Arch: Realtime IRQ thread system tuning.
 * schedtool - *schedtool* en Arch: Query or alter a process' scheduling policy
 * tuna - *tuna* en Arch: Thread and IRQ affinity setting GUI and cmd line tool

Instalación : `sudo pacman -S rt-tests sysstat iotop rtirq schedtool tuna`
